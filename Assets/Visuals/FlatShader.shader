﻿Shader "Custom/FlatShader" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _LC("LC", Color) = (1,0,0,0)
        _LP("LP", Vector) = (1,1,1,1)
    }
   
    SubShader {
        Tags { "Queue" = "Geometry" }
       
        Pass {
           
            GLSLPROGRAM
           
            #ifdef VERTEX
           
            #extension GL_EXT_gpu_shader4 : require

flat varying vec3  normal;        // note 'flat varying'

            const float SpecularContribution = 0.3;
            const float DiffuseContribution  = 1.0 - SpecularContribution;
 
            uniform vec4 _LP;
            flat varying vec2 TextureCoordinate;
            flat varying float LightIntensity;


void main(){
        normal = gl_NormalMatrix * gl_Normal;
        gl_Position = ftransform();
		
                gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
                TextureCoordinate = gl_MultiTexCoord0.xy;
               
                vec3 ecPosition = vec3(gl_ModelViewMatrix * gl_Vertex);
                vec3 tnorm = normalize(gl_NormalMatrix * gl_Normal);
                vec3 lightVec = normalize(_LP.xyz - ecPosition);
               
                vec3 reflectVec = reflect(-lightVec, tnorm);
                vec3 viewVec    = normalize(-ecPosition);
                float diffuse   = max(dot(lightVec, tnorm), 0.0);
               
                float spec      = 0.0;
                if (diffuse > 0.0)
                {
                    spec = max(dot(reflectVec, viewVec), 0.0);
                    spec = pow(spec, 16.0);
                }
               
                LightIntensity = DiffuseContribution * diffuse + SpecularContribution * spec;
}
           
            #endif
           
            #ifdef FRAGMENT
                       
#extension GL_EXT_gpu_shader4 : require

flat varying vec3  normal;        // note 'flat varying'
            uniform sampler2D _MainTex;
            varying vec2 TextureCoordinate;
            uniform vec4 _LC;
            flat varying float LightIntensity;

void main(){
        vec3 N = normalize(normal); 
		vec4 c;
		c.xyz = texture2D(_MainTex, TextureCoordinate).xyz * _LC.xyz;
        c *= LightIntensity;
        c.w = 1;

        // voila, we can use our flat normal! This will colour our fragment with the current normal for debug purposes.
        //gl_FragColor = (vec4(N, 1.0) + c) / 2.0;
		gl_FragColor = c + vec4(N, 1.0) / 2.0;
}
           
            #endif
           
            ENDGLSL
        }
    }
}