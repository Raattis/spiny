﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SpineMesh;

[System.Serializable]
public class InputSpine : SpineMesh.ISpine
{
    public AnimationCurve lengthShape;
    public float length = 1f;

    public bool IsLoop
    {
        get
        {
            return false;
        }
        set { }
    }

    public bool IsCloseEnded
    {
        get
        {
            return true;
        }
        set { }
    }

    public bool IsCloseStarted
    {
        get
        {
            throw new System.NotImplementedException();
        }
        set
        {
        }
    }

    public Vector3 GetPosition(float p)
    {
        //float t = Time.time;
        //return new Vector3(Mathf.Sin(p * 2 + t * 2) * 0.1f, Mathf.Cos(p * 2 + t) * 0.1f, (p + Mathf.Sin(p + t) * 0.1f) * 5);
        return new Vector3(0, 0, lengthShape.Evaluate(p) * length);
    }
}

[System.Serializable]
public class InputProfile : SpineMesh.IProfile
{
    public AnimationCurve lenghtWiseShape;
    public AnimationCurve lenghtWiseRotation;
    public AnimationCurve macroShape;
    public float macroShapeMultiplier = 1;
    public bool macroShapeMirror = true;
    public AnimationCurve microShape;
    public float microShapeMultiplier = 4;
    public bool microShapeMirror = false;


    public bool IsLoop
    {
        get
        {
            return true;
        }
        set { }
    }

    public Vector3 GetPosition(float p, float l)
    {
        //float t = Time.time;
        //float r = (Mathf.Sin(p * Mathf.PI * 8) * 0.5f + 1f) * Mathf.Sqrt(Mathf.Sin(l * Mathf.PI) * (1 + 0.5f * Mathf.Sin(l * Mathf.PI + Time.time * 3))) * (1 + 0.1f * Mathf.Sin(l * 9 + t));
        //float rota = Mathf.Sin(t*0.5f + l) * Mathf.PI * 0.5f;
        //return new Vector3(Mathf.Sin(p * Mathf.PI * 2 + rota) * r, Mathf.Cos(p * Mathf.PI * 2 + rota) * r, 0);
        float r = (macroShape.Evaluate((p * macroShapeMultiplier) % 1) + microShape.Evaluate((p * microShapeMultiplier) % 1)) * lenghtWiseShape.Evaluate(l);
        float rota = (p + lenghtWiseRotation.Evaluate(l)) * Mathf.PI * 2;
        return new Vector3(Mathf.Sin(rota) * r, -Mathf.Cos(rota) * r, 0);
    }
}

[System.Serializable]
public class InputAppendageBase : SpinalMesh<InputSpine, InputProfile>
{
    public InputAppendageBase()
    {
        spine = new InputSpine();
        profile = new InputProfile();
    }
}

public class InputAppendage : MonoBehaviour
{
    public InputAppendageBase appendage = new InputAppendageBase();

    void Update()
    {

    }
}