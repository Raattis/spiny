﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SpineMesh
{
    public static class SpineFunctions
    {
        private static Dictionary<int, int[]> bcLookUpTable = new Dictionary<int, int[]>();

        /// <summary>
        /// Formula: n! / k!(n-k)!
        /// </summary>
        /// <param name="n"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static float BinCoef(int n, int k)
        {
            if (k > n / 2) k = n - k;

            // Check look up table for values
            int[] table;
            if (bcLookUpTable.TryGetValue(n, out table))
            {
                if (table[k] != 0)
                {
                    return table[k];
                }
            }
            else
            {
                table = new int[n / 2 + 1];
                bcLookUpTable[n] = table;
            }

            int nk = n - k;

            // Count three factorials at once.
            int n_sum = 1;
            int k_sum = 1;
            int nk_sum = 1;

            for (int i = 2; i <= n; ++i)
            {
                n_sum *= i;
                if (k == i)
                    k_sum = n_sum;
                if (nk == i)
                    nk_sum = n_sum;
            }

            // Add value to look up table
            table[k] = n_sum / (k_sum * nk_sum);
            return table[k];
        }

        public static Vector3 GetBezier(float t, params Vector3[] nodes)
        {
            if (nodes.Length == 0) return Vector3.zero;

            Vector3 result = Vector3.zero;
            int n = nodes.Length - 1;

            for (int i = 0; i <= n; ++i)
            {
                float bc = BinCoef(n, i);
                result += bc * Mathf.Pow(1 - t, n - i) * Mathf.Pow(t, i) * nodes[i];
            }
            return result;
        }
    }

    public interface IThickness
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p">[0..1]</param>
        /// <returns></returns>
        float GetThickness(float p);
    }

    public interface ISpine
    {
        bool IsLoop { get; set; }
        bool IsCloseEnded { get; set; }
        bool IsCloseStarted { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p">[0..1]</param>
        /// <returns></returns>
        Vector3 GetPosition(float p);
    }

    public interface IProfile
    {
        bool IsLoop { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p">[0..1]</param>
        /// <param name="l">[0..1]</param>
        /// <returns></returns>
        Vector3 GetPosition(float p, float l);
    }

    public class ProfilePosition
    {
        public IProfile profile;
        private float position;

        public ProfilePosition(IProfile profile, float position)
        {
            this.profile = profile;
            this.position = position;
        }
    }

    public class MorphingProfile : IProfile
    {
        public List<ProfilePosition> profiles;
        public bool IsLoop { get { return false; } set { } }
        public MorphingProfile(ProfilePosition pp)
        {
            profiles = new List<ProfilePosition>();
            profiles.Add(pp);
        }

        public MorphingProfile(IProfile p, float position)
        {
            profiles = new List<ProfilePosition>();
            profiles.Add(new ProfilePosition(p, position));
        }

        public MorphingProfile(List<ProfilePosition> pps)
        {
            profiles = pps;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="radial">position on profile</param>
        /// <param name="length">position "length" wise</param>
        /// <returns></returns>
        public Vector3 GetPosition(float radial, float length)
        {
            Vector3 result = Vector3.zero;
            int n = profiles.Count;
            for (int i = 0; i < n; ++i)
            {
                //IProfile profile = profiles[i].profile;

                float bc = SpineFunctions.BinCoef(n, i);
                result += bc * Mathf.Pow(1 - length, n - i) * Mathf.Pow(length, i) * profiles[i].profile.GetPosition(radial, length);
            }
            return result;
        }
    }

    [System.Serializable]
    public abstract class SpinalMesh<S, P> : SpinalMesh
        where S : ISpine
        where P : IProfile
    {
        public S spine;
        public P profile;

        public SpinalMesh ToBase()
        {
            SpinalMesh result = this;
            result.spine = this.spine;
            result.profile = this.profile;
            return result;
        }
    }

    [System.Serializable]
    public abstract class SpinalMesh
    {
        public ISpine spine;
        public IProfile profile;

        public Vector3 GetPosition(float radialWise, float lengthWise)
        {
            return spine.GetPosition(lengthWise) + profile.GetPosition(radialWise, lengthWise);
        }

        public Vector3 GetNormal(float radialWise, float lengthWise)
        {
            Vector3 pp = GetPosition(radialWise, lengthWise);
            Vector3 tx = GetPosition(radialWise + 0.0001f, lengthWise) - pp;
            Vector3 ty = GetPosition(radialWise, lengthWise + 0.0001f) - pp;
            return Vector3.Cross(tx.normalized, ty.normalized);
        }
    }
}