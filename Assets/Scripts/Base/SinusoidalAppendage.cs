﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SpineMesh;

public class SinusoidalSpine : SpineMesh.ISpine
{
    public bool IsLoop
    {
        get
        {
            return false;
        }
        set { }
    }

    public bool IsCloseEnded
    {
        get
        {
            return true;
        }
        set { }
    }

    public bool IsCloseStarted
    {
        get
        {
            throw new System.NotImplementedException();
        }
        set
        {
        }
    }

    public Vector3 GetPosition(float p)
    {
        float t = Time.time;
        return new Vector3(Mathf.Sin(p * 2 + t * 2) * 0.1f, Mathf.Cos(p * 2 + t) * 0.1f, (p + Mathf.Sin(p + t) * 0.1f) * 5);
    }
}

public class SinusoidalProfile : SpineMesh.IProfile
{
    public bool IsLoop
    {
        get
        {
            return true;
        }
        set { }
    }

    public Vector3 GetPosition(float p, float l)
    {
        float t = Time.time;
        float r = (Mathf.Sin(p * Mathf.PI * 8) * 0.5f + 1f) * Mathf.Sqrt(Mathf.Clamp01(Mathf.Sin(l * Mathf.PI)) * (1 + 0.5f * Mathf.Sin(l * Mathf.PI + Time.time * 3))) * (1 + 0.1f * Mathf.Sin(l * 9 + t));
        float rota = Mathf.Sin(t*0.5f + l) * Mathf.PI * 0.5f;
        return new Vector3(Mathf.Sin(p * Mathf.PI * 2 + rota) * r, Mathf.Cos(p * Mathf.PI * 2 + rota) * r, 0);
    }
}

public class SinusoidalAppendage : SpinalMesh
{
    public SinusoidalAppendage()
    {
        spine = new SinusoidalSpine();
        profile = new SinusoidalProfile();
    }
}