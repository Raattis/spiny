﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SpineMesh;

public class FloppySpine : SpineMesh.ISpine
{
    public Vector3 currentPosition;
    public Vector3 endPosition;
    public Vector3 startPosition;

    public FloppySpine()
    {
        currentPosition = Vector3.zero;
    }

    public bool IsLoop
    {
        get
        {
            return false;
        }
        set { }
    }

    public bool IsCloseEnded
    {
        get
        {
            return true;
        }
        set { }
    }

    public bool IsCloseStarted
    {
        get
        {
            throw new System.NotImplementedException();
        }
        set
        {
        }
    }
    public void Update(Vector3 startPosition, Vector3 endPosition)
    {
        this.endPosition = endPosition;
        currentPosition = Vector3.Lerp(currentPosition, endPosition + startPosition, 0.1f);
    }

    public Vector3 GetPosition(float p)
    {
        return SpineFunctions.GetBezier(p, Vector3.zero, endPosition * 0.5f, currentPosition - startPosition);

        //return Vector3.Lerp(Vector3.zero, currentPosition, p);
        //float t = Time.time;
        //return new Vector3(Mathf.Sin(p * 2 + t * 2) * 0.1f, Mathf.Cos(p * 2 + t) * 0.1f, (p + Mathf.Sin(p + t) * 0.1f) * 5);
    }
}

public class FloppyProfile : SpineMesh.IProfile
{
    public bool IsLoop
    {
        get
        {
            return true;
        }
        set { }
    }

    public Vector3 GetPosition(float p, float l)
    {
        float t = Time.time;
        float r = Mathf.Sin((Mathf.Abs(l) * (Mathf.PI-0.2f)+0.2f)) * 0.2f;
        return new Vector3(Mathf.Sin(-p * Mathf.PI * 2) * r, Mathf.Cos(-p * Mathf.PI * 2) * r, 0);
        
    }
}

public class FloppyAppendage : SpinalMesh<FloppySpine, FloppyProfile>
{
    Transform self;
    Vector2 position;
    SpinalMesh parent;

    public FloppyAppendage(SpinalMesh parent, Transform self, Vector2 position)
    {
        this.self = self;
        this.parent = parent;
        this.position = position;
        spine = new FloppySpine();
        profile = new FloppyProfile();

        self.localPosition = parent.GetPosition(position.x, position.y);
        spine.startPosition = self.localPosition;
        spine.endPosition = parent.GetNormal(position.x, position.y);
        spine.currentPosition = spine.endPosition * 0.5f;
    }

    public void Update()
    {
        self.localPosition = parent.GetPosition(position.x, position.y);
        spine.Update(self.localPosition, parent.GetNormal(position.x, position.y) - self.localPosition * 0.5f);
    }
}