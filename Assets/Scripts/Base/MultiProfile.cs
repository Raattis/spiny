﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SpineMesh;

[System.Serializable]
public class BezierSpine : SpineMesh.ISpine
{
    #region heritance constants
    public bool IsLoop
    {
        get
        {
            return false;
        }
        set { }
    }

    public bool IsCloseEnded
    {
        get
        {
            return false;
        }
        set { }
    }

    public bool IsCloseStarted
    {
        get
        {
            return false;
        }
        set
        {
        }
    }
    #endregion

    //TODO: Replace with BezierCurve
    public AnimationCurve lengthShape;
    public float length = 1f;

    public Vector3 GetPosition(float p)
    {
        return new Vector3(0, 0, lengthShape.Evaluate(p) * length);
    }
}

[System.Serializable]
public class MultiProfile : SpineMesh.IProfile
{
    public bool IsLoop
    {
        get
        {
            return true;
        }
        set { }
    }

    public AnimationCurve lenghtWiseThickness;
    public List<SingleProfile> profiles;
    public enum ProfileBlendType
    {
        additive, multiply
    }
    public ProfileBlendType profileBlend;
    public enum EndCloseType
    {
        none, start, end, both
    }
    public EndCloseType endClosed;

    public Vector3 GetPosition(float p, float l)
    {
        switch (endClosed)
        {
            case EndCloseType.both:
                if (l == 0 || l == 1) return Vector3.zero;
                break;
            case EndCloseType.start:
                if (l == 0) return Vector3.zero;
                break;
            case EndCloseType.end:
                if (l == 1) return Vector3.zero;
                break;
        }

        Vector3 result;

        if (profileBlend == ProfileBlendType.additive)
        {
            result = Vector3.zero;
            for (int i = 0; i < profiles.Count; ++i)
                result += profiles[i].GetPosition(p, l);
        }
        else
        {
            result = new Vector3(Mathf.Sin(p * Mathf.PI * 2), -Mathf.Cos(p * Mathf.PI * 2), 0);
            for (int i = 0; i < profiles.Count; ++i)
            {
                Vector3 t = profiles[i].GetPositionWithoutThickness(p, l);
                float f = profiles[i].lenghtWiseInfluence.Evaluate(l);
                result = Vector3.Lerp(result, result * t.magnitude, f);
                //result = Vector3.Lerp(result, new Vector3(result.x * t.x, result.y * t.y, result.z * t.z), f);
            }
        }
        if (profiles.Count == 0) return new Vector3(Mathf.Sin(p * Mathf.PI * 2), -Mathf.Cos(p * Mathf.PI * 2), 0) * lenghtWiseThickness.Evaluate(l);
        return result * lenghtWiseThickness.Evaluate(l);
    }

    public void AddProfile()
    {
        profiles.Add(new SingleProfile(true));
    }
}

[System.Serializable]
public class SingleProfile : SpineMesh.IProfile
{
    public AnimationCurve lenghtWiseInfluence;
    public AnimationCurve lenghtWiseRotation;
    public AnimationCurve shape;
    public bool mirror = true;
    public float repeat = 2;

    public SingleProfile()
    {
    }
    public SingleProfile(bool init)
    {
        if (init == true)
        {
            lenghtWiseInfluence = new AnimationCurve();
            lenghtWiseInfluence.AddKey(new Keyframe(0, 1, 0, 0));
            lenghtWiseInfluence.AddKey(new Keyframe(1, 1, 0, 0));
            lenghtWiseRotation = new AnimationCurve();
            lenghtWiseRotation.AddKey(new Keyframe(0, 0, 0, 0));
            lenghtWiseRotation.AddKey(new Keyframe(1, 0, 0, 0));
            shape = new AnimationCurve();
            shape.AddKey(new Keyframe(0, 1, 0, 0));
            shape.AddKey(new Keyframe(1, 1, 0, 0));
        }
    }

    public bool IsLoop
    {
        get
        {
            return true;
        }
        set { }
    }

    public Vector3 GetPosition(float p, float l)
    {
        float r = (shape.Evaluate((p * repeat) % 1)) * lenghtWiseInfluence.Evaluate(l);
        float rota = (p + lenghtWiseRotation.Evaluate(l)) * Mathf.PI * 2;
        return new Vector3(Mathf.Sin(rota) * r, -Mathf.Cos(rota) * r, 0);
    }
    public Vector3 GetPositionWithoutThickness(float p, float l)
    {
        float r = (shape.Evaluate((p * repeat) % 1));
        float rota = (p + lenghtWiseRotation.Evaluate(l)) * Mathf.PI * 2;
        return new Vector3(Mathf.Sin(rota) * r, -Mathf.Cos(rota) * r, 0);
    }
}

[System.Serializable]
public class MultiProfileSpine : SpinalMesh<BezierSpine, MultiProfile>
{
    public MultiProfileSpine()
    {
        spine = new BezierSpine();
        profile = new MultiProfile();
    }
}
