﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SpineMesh;

public class TentacleFloater : MonoBehaviour
{
    public SpinalMeshMaker spinal;
    public Material material;
    Vector2 resolution = new Vector2(4,2);
    FloppyAppendage[,] cubes;

    void Start()
    {
        cubes = new FloppyAppendage[Mathf.RoundToInt(resolution.x), Mathf.RoundToInt(resolution.y)];
        for (int i = 0; i < resolution.x; ++i)
        {
            for (int j = 0; j < resolution.y; ++j)
            {
                Transform cube = new GameObject(i + ", " + j).transform;
                cube.parent = transform;
                cubes[i, j] = new FloppyAppendage(spinal.meshData, cube, new Vector2((i / (resolution.x) + 0.07f) % 1, (j + 0.5f) / (resolution.y + 0.5f)));
                SpinalMeshMaker smm = cube.gameObject.AddComponent<SpinalMeshMaker>();
                smm.meshData = cubes[i, j];
                smm.meshData.spine = cubes[i, j].spine;
                smm.meshData.profile = cubes[i, j].profile;
                smm.lengthReso = 32;
                smm.circReso = 8;
                smm.material = material;
            }
        }
    }

    void Update()
    {
        for (int i = 0; i < resolution.x; ++i)
        {
            for (int j = 0; j < resolution.y; ++j)
            {
                cubes[i, j].Update();
            }
        }
    }
}