﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SpineMesh;

public enum AppendageType
{
    Sinusoidal,
    Input,
    MultiProfile,
    Floppy
}

public class SpinalMeshMaker : MonoBehaviour
{
    public AppendageType appendage = AppendageType.Sinusoidal;
    public SpinalMesh meshData;
    MeshFilter mf;
    public Material material;
    public bool suspendUpdating = false;
    public bool reInit = false;

    public int lengthReso = 64;
    public int circReso = 128;
    public bool outerFaces = true;
    public bool innerFaces = false;
    public bool rotateUVs = true;
    public bool flatSurfaces = true;

    void Awake()
    {
        Init();
    }
    void Start()
    {
        InitMesh();
    }

    void Update()
    {
        if (reInit)
        {
            reInit = false;
            InitMesh();
        }
        else if (!suspendUpdating)
        {
            UpdateMesh();
        }
    }

    public void Init()
    {
        if (meshData == null)
        {
            if (appendage == AppendageType.Sinusoidal)
            {
                meshData = new SinusoidalAppendage();
            }
            else if (appendage == AppendageType.Input)
            {
                InputAppendage inputAppendage;
                if ((inputAppendage = GetComponent<InputAppendage>()) == null)
                {
                    inputAppendage = gameObject.AddComponent<InputAppendage>();
                }
                meshData = inputAppendage.appendage.ToBase();
                Debug.Log(meshData.spine);
            }
            else if (appendage == AppendageType.MultiProfile)
            {
                MultiProfileMono multiAppendage;
                if ((multiAppendage = GetComponent<MultiProfileMono>()) == null)
                {
                    multiAppendage = gameObject.AddComponent<MultiProfileMono>();
                }
                meshData = multiAppendage.appendage.ToBase();
            }
            else
            {
                Debug.LogError("Unknown appendage type.");
            }
        }
    }

    public void InitMesh()
    {
        if (meshData == null)
            Init();
        Vector3[] vertices = new Vector3[lengthReso * circReso];
        Vector3[] normals = new Vector3[vertices.Length];
        Vector2[] UVs = new Vector2[vertices.Length];
        int[] triangles;
        if (innerFaces != outerFaces)
            triangles = new int[vertices.Length * 6 - 12];
        else
            triangles = new int[(vertices.Length * 6 - 12) * 2];
        for (int l = 0; l < lengthReso; l++)
        {
            float lengthProgress = l / (float)(lengthReso - 1);
            for (int d = 0; d < circReso; d++)
            {
                float radialProgress = d / (float)(circReso - 1);
                Vector3 v = meshData.GetPosition(radialProgress, lengthProgress);

                int i = l * circReso + d % circReso;
                vertices[i] = v;
                if (l < lengthReso - 1 && d < circReso - 1)
                {
                    int e = l * circReso + d;
                    int f = l * circReso + d + 1;
                    int h = f + circReso;
                    int g = e + circReso;

                    int j = (innerFaces && outerFaces ? i * 12 : i * 6);
                    int k = 0;

                    if (innerFaces)
                    {
                        triangles[j + k++] = h;
                        triangles[j + k++] = f;
                        triangles[j + k++] = e;

                        triangles[j + k++] = g;
                        triangles[j + k++] = h;
                        triangles[j + k++] = e;
                    }
                    if (outerFaces)
                    {

                        triangles[j + k++] = f;
                        triangles[j + k++] = h;
                        triangles[j + k++] = e;

                        triangles[j + k++] = h;
                        triangles[j + k++] = g;
                        triangles[j + k++] = e;
                    }
                }
                if (rotateUVs)
                {
                    UVs[i] = new Vector2(lengthProgress, radialProgress);
                }
                else
                {
                    UVs[i] = new Vector2(radialProgress, lengthProgress);
                }
            }
        }

        MeshRenderer r;
        if ((r = gameObject.GetComponent<MeshRenderer>()) == null)
        {
            r = gameObject.AddComponent<MeshRenderer>();
            r.sharedMaterial = material;
        }

        if ((mf = gameObject.GetComponent<MeshFilter>()) == null)
            mf = gameObject.AddComponent<MeshFilter>();
        Mesh m = mf.sharedMesh = new Mesh();
        m.name = gameObject.name + "_mesh";
        m.vertices = vertices;
        m.uv = UVs;
        m.triangles = triangles;
        m.normals = CalculateNormals(vertices);
        m.RecalculateNormals();
        m.RecalculateBounds();
    }

    public void UpdateMesh()
    {
        if (meshData == null) return;
        Vector3[] vertices = new Vector3[lengthReso * circReso];
        for (int l = 0; l < lengthReso; l++)
        {
            float lengthProgress = l / (float)(lengthReso - 1);
            for (int d = 0; d < circReso; d++)
            {
                float radialProgress = d / (float)(circReso - 1);
                int i = l * circReso + d % circReso;
                vertices[i] = meshData.GetPosition(radialProgress, lengthProgress);
            }
        }
        Mesh m = mf.sharedMesh;
        m.vertices = vertices;
        m.RecalculateNormals();
        m.normals = CalculateNormals(vertices);
    }

    private Vector3[] CalculateNormals(Vector3[] vertices)
    {
        Vector3[] normals = new Vector3[vertices.Length];
        for (int l = 0; l < lengthReso - 1; l++)
        {
            float lengthProgress = l / (float)(lengthReso - 1);
            for (int d = 0; d < circReso; ++d)
            {
                int i = l * circReso + d;
                int j = l * circReso + (d + 1) % circReso;
                int k = (l + 1) * circReso + d;
                float radialProgress = d / (float)(circReso - 1);

                normals[i] = Vector3.Cross(vertices[j] - vertices[i], vertices[k] - vertices[i]).normalized;
            }
        }
        return normals;
    }

    public Vector3 GetPosition(float lengthWise, float radialWise)
    {
        Vector3 p = meshData.GetPosition(radialWise, lengthWise);
        return transform.position + (Vector3)(transform.localToWorldMatrix * p);
    }
    public Vector3 GetNormal(float lengthWise, float radialWise)
    {
        Vector3 p = meshData.GetNormal(radialWise, lengthWise);
        return transform.localToWorldMatrix * p;
    }
}