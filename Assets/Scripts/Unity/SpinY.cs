﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpinY : MonoBehaviour
{
    void Update()
    {
        transform.rotation = Quaternion.AngleAxis(Time.time * 20f, Vector3.up);
    }
}