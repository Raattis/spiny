﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class GenerateAtEditorTime : MonoBehaviour
{
    public bool generateContinuously = false;
    public bool generateOnce = false;
    public bool initialize = false;
    public SpinalMeshMaker smm;

    public bool destroyOnPlay = true;

    void Start()
    {
        if (destroyOnPlay && Application.isPlaying) Destroy(this);
        smm = GetComponent<SpinalMeshMaker>();
        smm.Init();
        smm.InitMesh();
    }

    public void Update()
    {
        if (initialize)
        {
            initialize = false;
            smm = GetComponent<SpinalMeshMaker>();
            smm.Init();
            smm.InitMesh();
        }
        else if (generateOnce)
        {
            generateOnce = false;
            smm.UpdateMesh();
        }
        else if (generateContinuously)
        {
            smm.UpdateMesh();
        }
    }
}