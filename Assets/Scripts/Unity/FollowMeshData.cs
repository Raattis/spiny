﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowMeshData : MonoBehaviour
{
    public SpinalMeshMaker spinal;
    public Vector2 pos;

    void Awake()
    {

    }

    void Update()
    {
        transform.position = spinal.GetPosition(pos.y, pos.x);
    }
}