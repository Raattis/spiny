﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CubeFloater : MonoBehaviour
{
    public SpinalMeshMaker spinal;
    public Material material;
    Vector2 resolution = new Vector2(32, 16);
    Transform[,] cubes;

    void Start()
    {
        cubes = new Transform[Mathf.RoundToInt(resolution.x), Mathf.RoundToInt(resolution.y)];
        for (int i = 0; i < resolution.x; ++i)
        {
            for (int j = 0; j < resolution.y; ++j)
            {
                cubes[i, j] = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
                cubes[i, j].localScale = Vector3.one * 0.1f + Vector3.up * 0.4f;
                cubes[i, j].parent = transform;
                cubes[i, j].renderer.material = material;
            }
        }
    }

    void Update()
    {
        for (int i = 0; i < resolution.x; ++i)
        {
            for (int j = 0; j < resolution.y; ++j)
            {
                Vector3 pos = spinal.GetPosition((j + 1) / (resolution.y + 2), i / resolution.x);
                Vector3 normal = spinal.GetNormal((j + 1) / (resolution.y + 2), i / resolution.x);
                cubes[i, j].position = pos;
                cubes[i, j].LookAt(pos + normal, Vector3.forward);
            }
        }
    }
}