﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using SpineMesh;

public enum PlaceHolder
{
    selection1,
    selection2,
    selection3
}

public class SpinyEditorWindow : EditorWindow
{
    //TODO: Initialization

    public static GameObject selected;
    public static SpinalMeshMaker meshMaker;
    public static MultiProfileMono multiMono;
    public static GenerateAtEditorTime autoGenerate;
    public MultiProfile multiProfile { get { return multiMono.appendage.profile; } }
    public BezierSpine bezierSpine { get { return multiMono.appendage.spine; } }
    public bool foldObjectProp = true;
    public bool foldLenghtwiseProp = true;
    public bool foldRadialProp = true;
    public bool foldStaticSettings = true;
    [MenuItem("Spiny/Window")]
    static void Init()
    {
        SpinyEditorWindow window = EditorWindow.GetWindow<SpinyEditorWindow>();
        window.OnSelectionChange();
    }

    void OnGUI()
    {
        if (selected == null || multiMono == null) OnSelectionChange();
        if (multiMono == null) return;

        GUIStyle bold = EditorStyles.foldout;
        bold.fontStyle = FontStyle.Bold;
        GUILayout.Label(selected.gameObject.name);
        if (Foldout(ref foldObjectProp, "Object properties", bold))
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(15);
            EditorGUILayout.BeginVertical();
            GUILayout.Label("Lenghtwise resolution");
            GUILayout.Label("Radial resolution");
            GUILayout.Label("UV rotate");
            GUILayout.Label("Profile blend type");
            GUILayout.Label("Closed ends");
            GUILayout.Label("3D type");
            if (true)
            {
                GUILayout.Label("Rotation");
            }
            else if (true)
            {
                GUILayout.Label("\nExtrusion direction");
            }
            else if (true)
            {
                GUILayout.Label("Wall thickness");
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            meshMaker.lengthReso = EditorGUILayout.IntField(meshMaker.lengthReso);
            meshMaker.circReso = EditorGUILayout.IntField(meshMaker.circReso - 1) + 1;
            meshMaker.rotateUVs = EditorGUILayout.Toggle(meshMaker.rotateUVs);
            if (GUI.changed && meshMaker.lengthReso > 2 && meshMaker.circReso > 2)
            {
                meshMaker.InitMesh();
                SceneView.RepaintAll();
            }
            GUI.changed = false;
            multiProfile.profileBlend = (MultiProfile.ProfileBlendType)EditorGUILayout.EnumPopup(multiProfile.profileBlend);
            multiProfile.endClosed = (MultiProfile.EndCloseType)EditorGUILayout.EnumPopup(multiProfile.endClosed);
            EditorGUILayout.EnumPopup(PlaceHolder.selection2);
            if (true)
            {
                EditorGUILayout.FloatField(355.5f);
            }
            else if (true)
            {
                EditorGUILayout.Vector3Field(":(", Vector3.one);
            }
            else if (true)
            {
                EditorGUILayout.FloatField(0.1f);
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
        }
        if (Foldout(ref foldLenghtwiseProp, "Lenghtwise properties", bold))
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(15);
            EditorGUILayout.BeginVertical();
            GUILayout.Label("Thickness");
            //GUILayout.Label("Rotation");
            GUILayout.Label("Z-position");
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            multiProfile.lenghtWiseThickness = EditorGUILayout.CurveField(multiProfile.lenghtWiseThickness);
            //multiProfile.lenghtWiseThickness = EditorGUILayout.CurveField(multiProfile.lenghtWiseThickness);
            bezierSpine.lengthShape = EditorGUILayout.CurveField(bezierSpine.lengthShape);
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
        }
        if (Foldout(ref foldRadialProp, "Radial properties", bold))
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(15);
            EditorGUILayout.BeginVertical();
            GUILayout.Label("");
            for (int i = 0; i < 1; ++i)
            {
                GUILayout.Label("Profile " + (i + 1));
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical();
            GUILayout.Label("Shape");
            for (int i = 0; i < multiProfile.profiles.Count; ++i)
            {
                multiProfile.profiles[i].shape = EditorGUILayout.CurveField(multiProfile.profiles[i].shape);
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(80));
            GUILayout.Label("Repeat");
            for (int i = 0; i < multiProfile.profiles.Count; ++i)
            {
                multiProfile.profiles[i].repeat = EditorGUILayout.FloatField(multiProfile.profiles[i].repeat);
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(40));
            GUILayout.Label("Mirror");
            for (int i = 0; i < multiProfile.profiles.Count; ++i)
            {
                multiProfile.profiles[i].mirror = EditorGUILayout.Toggle(multiProfile.profiles[i].mirror);
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical();
            GUILayout.Label("Lwise rotation");
            for (int i = 0; i < multiProfile.profiles.Count; ++i)
            {
                multiProfile.profiles[i].lenghtWiseRotation = EditorGUILayout.CurveField(multiProfile.profiles[i].lenghtWiseRotation);
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical();
            GUILayout.Label("Lwise influence");
            for (int i = 0; i < multiProfile.profiles.Count; ++i)
            {
                multiProfile.profiles[i].lenghtWiseInfluence = EditorGUILayout.CurveField(multiProfile.profiles[i].lenghtWiseInfluence);
            }
            EditorGUILayout.EndVertical();
            if (multiProfile.profiles.Count > 1)
            {
                EditorGUILayout.BeginVertical(GUILayout.MaxWidth(40));
                GUILayout.Label("Delete");
                for (int i = 0; i < multiProfile.profiles.Count; ++i)
                {
                    if (GUILayout.Button("X"))
                    {
                        Debug.Log("Deleted Profile" + (i + 1));
                        multiProfile.profiles.RemoveAt(i);
                    }
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(15);
            if (GUILayout.Button("Add profile", EditorStyles.miniButtonRight, GUILayout.MaxWidth(70)))
            {
                multiProfile.AddProfile();
                Debug.Log("Added a profile");
            }
            EditorGUILayout.EndHorizontal();

        }
        if (Foldout(ref foldStaticSettings, "Static settings", bold))
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(15);
            EditorGUILayout.BeginVertical();
            EditorGUILayout.Toggle(true, GUILayout.Width(10));
            EditorGUILayout.Toggle(true, GUILayout.Width(10));
            autoGenerate.generateContinuously = EditorGUILayout.Toggle(autoGenerate.generateContinuously, GUILayout.Width(10));
            if (!autoGenerate.generateContinuously && GUILayout.Button("Refresh mesh"))
            {
                meshMaker.UpdateMesh();
            }
            if (GUILayout.Button("Initialize mesh"))
            {
                meshMaker.InitMesh();
            }
            EditorGUILayout.EndVertical();
            GUILayout.Space(5);
            EditorGUILayout.BeginVertical();
            GUILayout.Label("Spine scene editor", GUILayout.MinWidth(10));
            GUILayout.Label("Auto-create MeshRenderer", GUILayout.MinWidth(10));
            GUILayout.Label("Generate mesh constantly", GUILayout.MinWidth(10));
            EditorGUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }
        if (GUI.changed)
        {
            meshMaker.UpdateMesh();
            SceneView.RepaintAll();
        }
    }

    void OnSelectionChange()
    {
        if (Selection.activeGameObject != null && (selected != Selection.activeGameObject || multiMono == null))
        {
            selected = Selection.activeGameObject;
            SpinalMeshMaker tempMaker = selected.GetComponent<SpinalMeshMaker>();
            MultiProfileMono tempMultiMono = selected.GetComponent<MultiProfileMono>();
            autoGenerate = selected.GetComponent<GenerateAtEditorTime>();
            if (tempMaker != null && tempMultiMono != null)
            {
                multiMono = tempMultiMono;
                meshMaker = tempMaker;
                Repaint();
            }
        }
    }

    static bool Foldout(ref bool toggled, string label, GUIStyle style)
    {
        GUILayout.BeginHorizontal();
        toggled = EditorGUI.Foldout(GUILayoutUtility.GetRect(new GUIContent(label), style, GUILayout.ExpandWidth(true)), toggled, label, true, style);
        GUILayout.EndHorizontal();
        return toggled;
    }
}
#endif